<?php

namespace Drupal\commerce_gc_client\Plugin\Commerce\ExchangerProvider;

use Drupal\commerce_exchanger\Plugin\Commerce\ExchangerProvider\ExchangerProviderRemoteBase;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\RequestOptions;

/**
 * Provides the transferwise.com exchange rates.
 *
 * @CommerceExchangerProvider(
 *   id = "gocardless",
 *   label = "GoCardless",
 *   display_label = "GoCardless",
 *   refresh_once = TRUE,
 *   enterprise = TRUE,
 * )
 */

class GoCardlessExchanger extends ExchangerProviderRemoteBase {

  /**
   * {@inheritdoc}
   */
  public function apiUrl() {}

  /**
   * {@inheritdoc}
   */
  public function getRemoteData($base_currency = NULL) {
    $data = NULL;
    $source = $this->isEnterprise() ? $base_currency : $this->getBaseCurrency();
    $partner = \Drupal::service('commerce_gc_client.gocardless_partner');
    foreach(['live', 'sandbox'] as $mode) {
      $result = $partner->api([
        'mode' => $mode,
        'endpoint' => 'currency_exchange_rates',
        'action' => 'list',
        'source' => $source,
      ]);
      if ($result && $result->response->status_code == 200) {
        $rates = $result->response->body->currency_exchange_rates;
        foreach ($rates as $rate) {
          $data[$rate->target] = (string) $rate->rate;
        }
        break;
      }
    }
    return $data;
  }

}
