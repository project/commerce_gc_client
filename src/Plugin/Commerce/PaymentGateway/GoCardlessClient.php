<?php

namespace Drupal\commerce_gc_client\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\commerce_gc_client\Event\GoCardlessEvents;
use Drupal\commerce_gc_client\Event\PaymentCreatedEvent;
use Drupal\commerce_gc_client\Event\PaymentNextEvent;
use Drupal\commerce_gc_client\Event\PaymentDetailsEvent;
use Drupal\commerce_gc_client\Event\SubsDetailsEvent;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the GoCardless Client payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "gocardless_client",
 *   label = "GoCardless Client",
 *   display_label = "Pay with GoCardless",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_gc_client\PluginForm\PaymentOffsiteForm",
 *   },
 *   modes = {
 *     "sandbox" = "Sandbox",
 *     "live" = "Live",
 *   },
 * )
 */
class GoCardlessClient extends OffsitePaymentGatewayBase {

  /**
   * The commerce_gc_client.gocardless_partner service.
   *
   * @var \Drupal\commerce_gc_client\GoCardlessPartner
   */
  protected $partner;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'currencies' => FALSE,
      'countries' => FALSE,
      'payment_limit' => 3,
      'email_warnings' => \Drupal::config('system.site')->get('mail'),
      'log_webhook' => FALSE,
      'log_api' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $mode = $this->configuration['mode'];
    $module_settings = \Drupal::config('commerce_gc_client.settings');

    // Test if the payment gateway has been saved.
    $payment_gateway = $form_state->getBuildInfo()['form_id'] == 'commerce_payment_gateway_edit_form' ? TRUE : FALSE;

    // If a the payment gateway has been saved test if there is an existing
    // connection with the GoCardless partner site.
    $connected = FALSE;
    if ($payment_gateway && $module_settings->get('org_id_' . $mode)) {
      $this->partner = \Drupal::service('commerce_gc_client.gocardless_partner');
      $this->partner->setGateway($form_state->getValue('id'));
      $auth = $this->partner->api([
        'mode' => $mode,
      ]);
      if ($auth == 200) {
        $connected = TRUE;
      }
    }

    // If the payment gateway has not been saved disable the options to toggle
    // the mode and to 'Connect' with the GoCardless partner site.
    if (!$payment_gateway) {
      $form['mode']['#disabled'] = TRUE;
      $connect_disabled = TRUE;
    }

    $connected ? $connect_submit = 'Disconnect' : $connect_submit = 'Connect';
    $mode == 'sandbox' ? $connect_value = $connect_submit . ' SANDBOX' : $connect_value = $connect_submit . ' LIVE';

    $form['mode']['#ajax'] = ['callback' => [$this, 'modeCallback']];

    $form['connect'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Connect with GoCardless'),
    ];
    // GoCardless verification status display, and onboarding link.
    if ($connected) {
      $result = $this->partner->api([
        'endpoint' => 'creditors',
        'action' => 'list',
      ]);
      if ($result && $result->response->status_code == 200) {
        $creditors = $result->response->body->creditors;
        if (!empty($creditors)) {
          $creditor = array_shift($creditors);
          if ($creditor->verification_status == 'successful') {
            $verified_markup = '<div class="messages messages--status">' . t('Your GoCardless account is verified.') . '</div>';
          }
          else {
            $verification_status = $creditor->verification_status == 'in_review' ? 'In review' : 'Action required';
            $client_url = Url::fromRoute('<current>', [], [
              "absolute" => TRUE,
            ])->toString();
            $partner_url = \Drupal::config('commerce_gc_client.settings')->get('partner_url');
            $onboarding_url = $partner_url . '/gc_partner/onboarding_direct?env=' . $mode . '&client_url=' . $client_url;
            $verified_markup = '<div class="messages messages--warning">' . t("Your GoCardless account verification status is '@verification_status'. <a href='@onboarding_url'>Click here</a> to complete GoCardless onboarding and get verified.", [
              '@onboarding_url' => $onboarding_url,
              '@verification_status' => $verification_status,
            ]) . '</div>';
          }
          $form['connect']['verified'] = [
            '#markup' => $verified_markup,
          ];
        }
      }
    }

    $markup = '<br /><p><b>Connect / disconnect with GoCardless</b></p>';
    if (!$connected) {
      $markup .= "<p>After clicking 'Connect' you will be redirected to the GoCardless where you can create an account and connect your site as a client of Seamless-CMS.co.uk</p>";
      $form['connect']['markup'] = [
        '#markup' => $markup,
      ];
    }

    if (isset($connect_disabled) && $connect_disabled) {
      $connect_suffix = '<br /><i>' . $this->t('Save the form before you can connect.') . '</i>';
    }
    elseif ($mode == 'live' && !isset($_SERVER['HTTPS'])) {
      $connect_disabled = TRUE;
      $connect_suffix = '<br /><i>' . $this->t('Site needs to be secure (https) before you can connect to GoCardless LIVE.') . '</i>';
    }

    $form['connect']['submit'] = [
      '#type' => 'submit',
      '#value' => $connect_value,
      '#submit' => [[$this, 'submit' . $connect_submit]],
      '#disabled' => isset($connect_disabled) ? $connect_disabled : FALSE,
      '#suffix' => isset($connect_suffix) ? $connect_suffix : NULL,
      '#attributes' => $connected ? ['onclick' => 'if (!confirm("Are you sure you want to disconnect your site from GoCardless?")) {return false;}'] : NULL,
    ];

    if ($connected) {
      if (!$webhook_secret = $module_settings->get('webhook_secret_' . $mode)) {
        $webhook_secret = $this->partner->api([
          'endpoint' => 'webhook_secret',
        ])->response;
        $settings = \Drupal::service('config.factory')->getEditable('commerce_gc_client.settings');
        $settings->set('webhook_secret_' . $mode, $webhook_secret)->save();
      }
      global $base_url;
      $webhook_url = $base_url . '/gc_client/webhook';
      $gc_webhook_url = $this->t('https://manage@env.gocardless.com/developers/webhook-endpoints', [
        '@env' => $mode == 'sandbox' ? '-sandbox' : '',
      ]);
      $webhook_secret_markup = "<p id='webhook_secret'><b>" . $this->t('Webhook secret:') . "</b> " . $webhook_secret . "</p>";
      $webhook_secret_markup .= '<p>' . $this->t('To receive webhooks create / update a Webhook Endpoint at your GoCardless account <a target="new" href="@gc_webhook_url">here</a>, and set the Webhook URL as <i>@webhook_url</i>, and the Webhook Secret as the random 30 byte string that has been generated for you above.', [
        '@webhook_url' => $webhook_url,
        '@gc_webhook_url' => $gc_webhook_url,
      ]) . '</p>';
      $form['connect']['webhook_secret_markup'] = [
        '#title' => $this->t('Webhook secret'),
        '#type' => 'markup',
        '#markup' => $webhook_secret_markup,
      ];
      $form['connect']['webhook_submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Change secret'),
        '#suffix' => $this->t('If you change this, and have already set up your webhook endpoint in your GoCardless account, you will need to update it there as well.'),
        '#ajax' => [
          'callback' => [$this, 'webhookSecretCallback'],
          'wrapper' => 'webhook_secret',
        ],
      ];
    }

    // Global.
    $form['global'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General settings'),
    ];

    $fx_payments = TRUE;
    if (!isset($creditor) || (
        isset($creditor) && !$creditor->fx_payout_currency)) { 
      $fx_payments = FALSE;
      $form['global']['currency_markup'] = [
        '#type' => 'markup',
        '#markup' => $this->t("The following two settings use GoCardless international payment features. If the checkboxes are disabled it is because you need to enable FX Payments on your GoCardless account before you can use them. To do this contact <a href='mailto:help@gocardless.com'>help@gocardless.com</a>."),
      ];
    }
    $form['global']['currencies'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Customer can choose currency at GoCardless'),
      '#default_value' => $this->configuration['currencies'],
      '#description' => $this->t("Only applies to Redirect Flows:- checkouts that do not result in an Instant Payment creation. Without this setting, customer's are restricted to creating debit mandates in the currency of their order. When this option is enabled the customer is able to choose from all the currencies that you have available at GoCardless. The amount of any payments that are created through the mandate are automatically adjusted using the latest, real exchange rates provided by GoCardless. It is not recommended that you use Subscription payment types if you have this option selected (or if you use the Commerce Currency Resolver module) - see the README file for more details. Go to <a href='/admin/commerce/config/currencies/gocardless'>here</a> for more options, and information on enabling multiple currencies with GoCardless."),
      '#disabled' => $fx_payments ? FALSE : TRUE,
    ];

    $form['global']['countries'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically select country for Instant Payments'),
      '#default_value' => $this->configuration['countries'],
      '#description' => $this->t("Only applies to Billing Request flows:- checkouts that result in the creation of an Instant Payment. With this enabled, the currency of the payment is determined by the customer's billing address country. If the currency of the country is different to the currency of the order then the payment amount will be adjusted using the latest real exchange rates provided by GoCardless. The range of countries that GoCardless provides Instant Payments for is currently limited. If the customer's country is not provided for then the checkout will fallback to using a debit mandate instead of an Instant Payment."),
      '#disabled' => $fx_payments ? FALSE : TRUE,
    ];

    $form['global']['payment_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum payments'),
      '#default_value' => $this->configuration['payment_limit'],
      '#size' => 3,
      '#min' => 1,
      '#step' => 1,
      '#description' => $this->t("The maximum number of payments that can be raised automatically, per order, per day. If the amount is exceeded, a warning email is sent to the specified address above. Leave unset for unlimitted."),
      '#required' => FALSE,
    ];

    $form['global']['email_warnings'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#default_value' => $this->configuration['email_warnings'],
      '#description' => $this->t("Email address to send warnings."),
      '#size' => 40,
      '#maxlength' => 40,
    ];

    // Logging options.
    $form['log'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Logging'),
    ];

    $form['log']['log_webhook'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#type' => 'checkbox',
      '#title' => '<b>' . $this->t('Enable webhook logging') . '</b>',
      '#description' => $this->t('Webhooks recieved from GoCardless will be written to the log.'),
      '#default_value' => $this->configuration['log_webhook'],
    ];

    $form['log']['log_api'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#type' => 'checkbox',
      '#title' => '<b>' . $this->t('Enable API logging') . '</b>',
      '#description' => $this->t('Responses from the Partner site to API posts will be written to the log.'),
      '#default_value' => $this->configuration['log_api'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['currencies'] = $values['global']['currencies'];
      $this->configuration['countries'] = $values['global']['countries'];
      $this->configuration['email_warnings'] = $values['global']['email_warnings'];
      $this->configuration['payment_limit'] = $values['global']['payment_limit'];
      $this->configuration['log_api'] = $values['log']['log_api'];
      $this->configuration['log_webhook'] = $values['log']['log_webhook'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    
    // Returning from a Billing Request flow.
    if (isset($_GET['billing_request_id']) && $request_id = $_GET['billing_request_id']) {
      // Call the GoC API to get customer ID and mandate ID if there is one.
      $payment_gateway = $order->get('payment_gateway')->first()->entity;
      $partner = \Drupal::service('commerce_gc_client.gocardless_partner');
      $partner->setGateway($payment_gateway->Id());
      $result = $partner->api([
        'endpoint' => 'billing_request',
        'action' => 'get',
        'id' => $request_id,
      ]);

      if ($result->response->status_code == 200) {
        $billing_request = $result->response->body->billing_requests;
        if (isset($billing_request->links->mandate_request_mandate)) {
          $mandate_id = $billing_request->links->mandate_request_mandate;
          $message = $this->t('Your new debit mandate @mandate has been created by GoCardless.', [
            '@mandate' => $mandate_id,
          ]);
          \Drupal::messenger()->addMessage($message);
        }
        elseif (isset($billing_request->links->mandate_request)) {
          $message = $this->t('A new debit mandate is being created for you by GoCardless.');
          \Drupal::messenger()->addMessage($message);
        }

        $customer_id = NULL;
        if (isset($billing_request->links->customer)) {
          $customer_id = $billing_request->links->customer;
        }
      }
      else {
        throw new PaymentGatewayException('Billing Request creation was unsuccessful with order.');
      }

      // Process the Instant Payment that has been created.
      if (isset($billing_request->payment_request)) {
        $payment = $billing_request->payment_request;
        $order_currency = $order->getTotalPrice()->getCurrencyCode();
        if ($order_currency != $payment->currency) {
          $currency_message = $this->t('The price of your order has been converted from @order_currency to @payment_currency using the latest, real currency exchange rates provided by GoCardless.', array(
            '@order_currency' => $order_currency,
            '@payment_currency' => $payment->currency,
          ));
          \Drupal::messenger()->addMessage($currency_message);
        }

        $payment_amount = $payment->amount / 100;
        $payment_message_amount = \Drupal::service('commerce_price.currency_formatter')->format($payment_amount, $payment->currency);
        if (!$billing_request->fallback_occurred) {
          $payment_message = $this->t('You have made an Instant payment of @amount via GoCardless for your order #@order_id.', array(
            '@order_id' => $order->id(),
            '@amount' => $payment_message_amount, 
          ));
        }
        else {
          $payment_message = $this->t('A payment of @amount has been created via GoCardless with your debit mandate and will be paid from your bank account in 3-4 working days.', array(
            '@amount' => $payment_message_amount, 
          ));
        }
        \Drupal::messenger()->addMessage($payment_message);
      }

      // Process any One-off payments if required.
      foreach ($order->getItems() as $item) {
        $item_id = $item->Id();
        if ($data = $item->getData('gc')) {
          if ($data['gc_type'] == 'P') {
            // Insert info about the order into the database.
            if (isset($mandate_id) && !isset($gcid)) {
              $payment_gateway = $order->get('payment_gateway')->first()->entity;
              $mode = $payment_gateway->get('configuration')['mode'];
              $created = \Drupal::time()->getRequestTime();
              $db = \Drupal::database();
              $gcid = $db->insert('commerce_gc_client')->fields([
                'order_id' => $order->id(),
                'gc_mandate_id' => $mandate_id,
                'gc_mandate_scheme' => $billing_request->mandate_request->scheme,
                'gc_mandate_status' => 'pending_submission',
                'created' => $created,
                'data' => serialize([
                  'gc_billing_request_id' => $request_id,
                  'gc_customer_id' => $billing_request->links->customer,
                ]),
                'sandbox' => $mode == 'sandbox' ? 1 : 0,
              ])->execute();
            }
              
            // Calculate an updated next_payment creation date for the order
            // item and insert to database.
            $next_payment = NULL;
            if (isset($data['interval_params'])) {
              $next_payment = strtotime('+' . $data['interval_params']['string']);
              $calculate = commerce_gc_client_price_calculate($order, $item);
              $amount = \Drupal::service('commerce_price.currency_formatter')
                ->format($calculate['amount'], $calculate['currency_code']);
              $message = t("The next recurring payment of @amount for '@product_title' has been scheduled for @next_payment_date.", array(
                '@amount' => $amount,
                '@product_title' => $item->getTitle(),
                '@next_payment_date' => \Drupal::service('date.formatter')->format($next_payment, 'gocardless_client'),
              ));
              \Drupal::messenger()->addMessage($message);
            }
            if (isset($gcid)) {
              $db->insert('commerce_gc_client_item')->fields([
                'item_id' => $item_id,
                'gcid' => $gcid,
                'type' => $data['gc_type'],
                'next_payment' => $next_payment,
              ])->execute();
            }
          }
        }
      }
    }

    //Returning from a Redirect flow.
    elseif (isset($_GET['mandate']) && $mandate_id = $_GET['mandate']) {
      $order_id = $order->id();
      $payment_gateway = $order->get('payment_gateway')->first()->entity;
      $partner = \Drupal::service('commerce_gc_client.gocardless_partner');
      $partner->setGateway($payment_gateway->Id());
      $uuid_service = \Drupal::service('uuid');
      $event_dispatcher = \Drupal::service('event_dispatcher');

      $result = $partner->api([
        'endpoint' => 'mandates',
        'action' => 'get',
        'mandate' => $mandate_id,
      ]);
      if ($result->response->status_code != 200) {
        throw new PaymentGatewayException('GoCardless mandate has not been created for order.');
      }
      else {
        $mandate = $result->response->body->mandates;
      }

      // Insert info about the order into the database.
      $mode = $payment_gateway->get('configuration')['mode'];
      $created = \Drupal::time()->getRequestTime();
      $db = \Drupal::database();
      $gcid = $db->insert('commerce_gc_client')->fields([
        'order_id' => $order_id,
        'gc_mandate_id' => $mandate_id,
        'gc_mandate_scheme' => $mandate->scheme,
        'gc_mandate_status' => 'pending_submission',
        'created' => $created,
        'data' => serialize([
          'gc_redirect_id' => NULL,
          'gc_customer_id' => $_GET['customer'],
        ]),
        'sandbox' => $mode == 'sandbox' ? 1 : 0,
      ])->execute();

      $comment = $this->t('Your new debit mandate @mandate has been created by GoCardless.', [
        '@mandate' => $mandate_id,
      ]);
      \Drupal::messenger()->addMessage($comment);

      // Set up seperate payments / subscriptions for each product item in
      // order.
      foreach($order->getItems() as $item) {
        $item_id = $item->Id();
        if (!$data = $item->getData('gc')) {
          $data = [];
        }
        if (empty($data)) {
          $data['gc_type'] = 'P';
        }

        // Payment type is subscription.
        if ($data['gc_type'] == 'S') {
          $calculate = commerce_gc_client_price_calculate($order, $item);
          $order_currency = $order->getTotalPrice()->getCurrencyCode();
          if ($order_currency != $calculate['currency_code']) {
            $currency_exchanged = TRUE;
          }
          $start_date = !empty($data['gc_start_date']) ? \Drupal::service('date.formatter')->format(strtotime($data['gc_start_date']), 'gocardless') : NULL;
          $end_date = !empty($data['gc_end_date']) ? \Drupal::service('date.formatter')->format(strtotime($data['gc_end_date']), 'gocardless') : NULL;

          $subs_details = [
            'endpoint' => 'subscriptions',
            'action' => 'create',
            'mandate' => $mandate_id,
            'amount' => $calculate['amount'],
            'currency' => $calculate['currency_code'],
            'name' => $this->t('Subscription for') . ' ' . $item->getTitle(),
            'interval' => $data['interval_params']['length'],
            'interval_unit' => $data['interval_params']['unit'],
            'start_date' => $start_date,
            'count' => !empty($data['gc_count']) ? $data['gc_count'] : NULL,
            'end_date' => $end_date,
            'day_of_month' => !empty($data['gc_dom']) ? $data['gc_dom'] : NULL,
            'month' => !empty($data['gc_month']) ? $data['gc_month'] : NULL,
            'metadata' => [
              'item_id' => $item_id,
            ],
            'idempotency_key' => $uuid_service->generate(),
          ];

          // Dispatch an event so that the subscription details array can be
          // altered by other modules before sending to GoCardless.
          $subs_details_event = new SubsDetailsEvent($subs_details, $item_id);
          $event_dispatcher->dispatch($subs_details_event, GoCardlessEvents::SUBS_DETAILS);
          $subs_details = $subs_details_event->getSubsDetails();
          $result = $partner->api($subs_details);

          if ($result->response->status_code == 201) {
            $sub = $result->response->body->subscriptions;
            $amount = \Drupal::service('commerce_price.currency_formatter')
              ->format($sub->amount / 100, $calculate['currency_code']);

            $comment_arr = [
              '@product' => $item->getTitle(),
              '@interval' => $sub->interval,
              '@interval_unit' => $sub->interval_unit,
              '@amount' => $amount,
              '@first_payment' => \Drupal::service('date.formatter')->format(strtotime(array_shift($sub->upcoming_payments)->charge_date), 'gocardless_client'),
            ];

            $comment = $this->t('Your @interval @interval_unit subscription of @amount for <b>@product</b> has been created with GoCardless, and the first payment will be made from your bank on @first_payment.', $comment_arr);
            \Drupal::messenger()->addMessage($comment);

            $db->insert('commerce_gc_client_item')->fields([
              'gcid' => $gcid,
              'item_id' => $item_id,
              'type' => $data['gc_type'],
              'gc_subscription_id' => $sub->id,
            ])->execute();
          }
          else {
            if (!isset($result->response) || !$result->response || $result->response->status_code == 500) {
              \Drupal::messenger()->addWarning($this->t('There was a problem creating your subscription at GoCardless, so we will resubmit it later.'));
              $db->insert('commerce_gc_client_item')->fields([
                'gcid' => $gcid,
                'item_id' => $item_id,
                'type' => $data['gc_type'],
                'next_payment' => $created,
              ])->execute();
              $data['subs_details'] = $subs_details;
              $item->setData('gc', $data);
            }
            else {
              \Drupal::messenger()->addError($this->t('Something went wrong creating your subscription with GoCardless. Please contact the site administrator for assistance.'));
            }
          }
        }

        // Payment type is One-off payment.
        elseif ($data['gc_type'] == 'P') {
          $db->insert('commerce_gc_client_item')->fields([
            'item_id' => $item_id,
            'gcid' => $gcid,
            'type' => $data['gc_type'],
          ])->execute();

          $next_payment = NULL;
          $first_payment = count($data) == 1 ? 1 : $this->startDateCalculate($data);

          // Dispatch an event so that the first_payment date can be altered by
          // other modules.
          $first_payment_event = new PaymentNextEvent($first_payment, $item_id, 'first_payment');
          $event_dispatcher->dispatch($first_payment_event, GoCardlessEvents::PAYMENT_NEXT);
          $first_payment = $first_payment_event->getNextPayment();

          // If the product is set to create the first payment immediately.
          if ($first_payment == 1) {
            $calculate = commerce_gc_client_price_calculate($order, $item);
            $payment_details = [
              'endpoint' => 'payments',
              'action' => 'create',
              'mandate' => $mandate_id,
              'amount' => $calculate['amount'],
              'currency' => $calculate['currency_code'],
              'description' => $this->t('Payment for') . ' ' . $item->getTitle(),
              'metadata' => [
                'item_id' => $item_id,
              ],
              'idempotency_key' => $uuid_service->generate(),
            ];

            // Dispatch an event so that the payment details array can be altered
            // by other modules before sending to GoCardless.
            $payment_details_event = new PaymentDetailsEvent($payment_details, $item_id, 'checkout');
            $event_dispatcher->dispatch($payment_details_event, GoCardlessEvents::PAYMENT_DETAILS);
            $payment_details = $payment_details_event->getPaymentDetails();

            // Create a payment.
            $result = $partner->api($payment_details);
            if ($result->response->status_code == 201) {
              $payment = $result->response->body->payments;
              if (isset($data['interval_params'])) {
                $next_payment = strtotime('+' . $data['interval_params']['string']);
              }
              $amount = \Drupal::service('commerce_price.currency_formatter')
                ->format($payment->amount / 100, $calculate['currency_code']);
              $comment_array = [
                '@product' => $item->getTitle(),
                '@amount' => $amount,
                '@charge_date' => \Drupal::service('date.formatter')->format(strtotime($result->response->body->payments->charge_date), 'gocardless_client'),
              ];
              $comment = $this->t('A payment of @amount for <b>@product</b> has been created and will be made from your bank on @charge_date.', $comment_array);
              \Drupal::messenger()->addMessage($comment);

              // Dispatch an event so that other modules can respond to payment
              // creation.
              $payment_created_event = new PaymentCreatedEvent($payment, $item_id, 'checkout');
              $event_dispatcher->dispatch($payment_created_event, GoCardlessEvents::PAYMENT_CREATED);
            }
            else {
              if (!isset($result->response) || !$result->response || $result->response->status_code == 500) {
                $next_payment = $created;
                $data['payment_details'] = $payment_details;
                $item->setData('gc', $data);
                \Drupal::messenger()->addWarning($this->t('There was a problem creating your initial payment at GoCardless, so we will resubmit it later.'));
              }
              else {
                \Drupal::messenger()->addWarning($this->t('Something went wrong creating your initial payment with GoCardless. Please contact the site administrator for assistance.'));
              }
            }
          }

          // Else if a first_payment date is set for the product then defer the
          // first payment creation.
          elseif ($first_payment) {
            $next_payment = $first_payment;
            $calculate = commerce_gc_client_price_calculate($order, $item);
            $initial_payment = \Drupal::service('commerce_price.currency_formatter')->format($calculate['amount'], $calculate['currency_code']);
            $comment = $this->t('A payment for @initial_payment will be created with GoCardless on @charge_date.', [
              '@initial_payment' => $initial_payment,
              '@charge_date' => \Drupal::service('date.formatter')->format(strtotime('midnight', $first_payment), 'gocardless_client'),
            ]);
            \Drupal::messenger()->addMessage($comment);
          }

          // Dispatch an event so that the next_payment date can be altered by
          // other modules. Todo could cause a problem if there is no response
          // or a 500 response on the payment creation, as the payment will not
          // not be submitted with the same idempotency key.
          $next_payment_event = new PaymentNextEvent($next_payment, $item_id, 'checkout');
          $event_dispatcher->dispatch($next_payment_event, GoCardlessEvents::PAYMENT_NEXT);
          $next_payment = $next_payment_event->getNextPayment();

          // Update values for next scheduled payment.
          $db->update('commerce_gc_client_item')
            ->fields(['next_payment' => $next_payment])
            ->condition('item_id', $item_id)->execute();
        }
        $item->save();
      }

      if (isset($calculate)) {
        $order_currency = $order->getTotalPrice()->getCurrencyCode();
        if ($order_currency != $calculate['currency_code']) {
          $currency_message = $this->t('The price of your order has been converted from @order_currency to @payment_currency using the latest, real currency exchange rates provided by GoCardless.', array(
            '@order_currency' => $order_currency,
            '@payment_currency' => $calculate['currency_code'],
          ));
          \Drupal::messenger()->addMessage($currency_message);
        }
      }

      $order->save();
    }
  }

  /**
   * Calculates first payment creation date for an order item.
   *
   * Returns TRUE if the product variation is set to create a payment
   * immediately, or a Unix timestamp if a first payment date has been
   * calculated.
   *
   * @param array $data
   *   The data array for the new order item.
   * @param string $date
   *   (optional) A date as a string. For testing purposes only.
   *
   * @return mixed
   *   TRUE or integer.
   */
  public static function startDateCalculate(array $data, $date = FALSE) {
    if ($data['gc_create_payment']) {
      return TRUE;
    }
    $time = $date ? strtotime($date) : strtotime('midnight');
    $start_time = $data['gc_start_date'] ? strtotime($data['gc_start_date']) : $time;

    // Weekly intervals.
    if ($data['gc_interval_unit'] == 'weekly') {
      $dow = date('l', $start_time);
      if ($dow == $data['gc_dow'] || !$data['gc_dow']) {
        $start_time > $time ? $fp = $start_time : $fp = $time;
      }
      else {
        $start_time < $time ? $start_time = $time : NULL;
        $fp = strtotime('next ' . $data['gc_dow'], $start_time);
      }
      return ($fp);
    }

    // Monthly intervals.
    elseif ($data['gc_interval_unit'] == 'monthly') {
      $data['gc_dom'] == -1 ? $string = 'last day of ' : $string = $data['gc_dom'];
      $start_time < $time ? $start_time = $time : NULL;
      $m = date('M', $start_time);
      $y = date('Y', $start_time);
      $fp = strtotime($string . $m . $y);
      if ($fp < $start_time) {
        $fp = strtotime('+1 month', $fp);
      }
      return $fp;
    }

    // Yearly intervals.
    elseif ($data['gc_interval_unit'] == 'yearly') {
      $data['gc_dom'] == -1 ? $string = 'last day of ' : $string = $data['gc_dom'];
      $y = date('Y', $start_time);
      $fp = strtotime($string . $data['gc_month'] . $y);
      if ($fp < $start_time) {
        $fp = strtotime('+1 year', $fp);
        // Take account of leap years.
        if (date('n', $fp) == 2 && $data['gc_dom'] == -1) {
          $fp = strtotime('last day of ' . date('M', $fp), $fp);
        }
      }
      return $fp;
    }

    // No interval set.
    elseif ($data['gc_start_date']) {
      $fp = strtotime($data['gc_start_date']);
      if ($fp > $time) {
        return $fp;
      }
    }
    return $time;
  }

  /**
   * Form submission handler for buildConfigurationForm().
   *
   * Redirect to Partner site to activate the GoCardless OAuth Flow.
   */
  public function submitConnect(array &$form, FormStateInterface $form_state) {
    global $base_url;
    $gateway_id = $form_state->getvalue('id');
    $client_url = urlencode($base_url . '/gc_client/connect_complete');
    $site_email = \drupal::config('system.site')->get('mail');
    $env = $form_state->getValue('configuration')['gocardless_client']['mode'];
    $partner_url = \Drupal::config('commerce_gc_client.settings')->get('partner_url');
    $token = \Drupal::service('password_generator')->generate(24);
    \Drupal::service('config.factory')->getEditable('commerce_gc_client.settings')->set('partner_pass_' . $env, $token)->save();
    $url = $partner_url . '/gc_partner/connect/' . $env . '?mail=' . $site_email . '&client_url=' . $client_url . '&gateway_id=' . $gateway_id . '&token=' . $token . '&module=Commerce 2.x';
    $response = new TrustedRedirectResponse($url);
    $form_state->setresponse($response);

    // Remove the 'desination=' parameter from query string if it exists.
    $request = \Drupal::request();
    $request->query->set('destination', NULL);
  }

  /**
   * Form submission handler for buildConfigurationForm().
   *
   * Disconnects client site from GC partner site.
   */
  public function submitDisconnect(array &$form, FormStateInterface $form_state) {
    $partner = \Drupal::service('commerce_gc_client.gocardless_partner');
    $partner->setGateway($form_state->getValue('id'));
    $result = $partner->api([
      'endpoint' => 'oauth',
      'action' => 'revoke',
    ]);
    if ($result->response == 200) {
      \Drupal::messenger()->addMessage($this->t('You have disconnected successfully from GoCardless'));
    }
    else {
      \Drupal::messenger()->addError($this->t('There was a problem disconnecting from GoCardless'));
    }

    // TODO The following should only happen if there are no other GoC payment
    // gateways installed.
    $session = \Drupal::request()->getSession();
    if ($session->get('commerce_gc_client_cookie_created')) {
      $session->remove('commerce_gc_client_cookie_created');
    }
    $mode = $form_state->getValue('configuration')['gocardless_client']['mode'];
    $config = \Drupal::service('config.factory')->getEditable('commerce_gc_client.settings');
    $config->set('org_id_' . $mode, NULL)->save();
    $config->set('partner_user_' . $mode, NULL)->save();
    $config->set('partner_pass_' . $mode, NULL)->save();

    // Remove the 'desination=' parameter from query string if it exists.
    $request = \Drupal::request();
    $request->query->set('destination', NULL);
  }

  /**
   * Ajax callback function.
   *
   * On changing 'mode', saves new configuration and reloads the page.
   *
   * @see buildConfigurationForm()
   */
  public function modeCallback(array &$form, FormStateInterface $form_state) {
    $gateway_id = $form_state->getValue('id');
    $mode = $form_state->getValue('configuration')['gocardless_client']['mode'];
    \Drupal::configFactory()->getEditable('commerce_payment.commerce_payment_gateway.' . $gateway_id)->set('configuration.mode', $mode)->save();

    if (empty($gateway_id)) {
      $path = '/admin/commerce/config/payment-gateways/add';
    }
    else {
      $path = '/admin/commerce/config/payment-gateways/manage/' . $gateway_id;
    }
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand($path));
    return $response;
  }

  /**
   * Ajax callback function.
   *
   * On submitting 'Change secret' a new client secret is obtained, saved to
   * the module config and returned for display on the settings form.
   *
   * @return array
   *   The new render array for display on the settings form.
   *
   * @see buildConfigurationForm()
   */
  public function webhookSecretCallback(array &$form, FormStateInterface $form_state) {
    \Drupal::messenger()->addWarning(t('You have changed your webhook secret. If you have already added a secret to your GoCardless account, you will have to update it there as well.'));
    $mode = $this->configuration['mode'];
    $webhook_secret = $this->partner->api([
      'endpoint' => 'webhook_secret',
    ])->response;
    $settings = \Drupal::service('config.factory')->getEditable('commerce_gc_client.settings');
    $settings->set('webhook_secret_' . $mode, $webhook_secret)->save();
    $output = "<p id='webhook_secret'><b>" . $this->t('Webhook secret:') . "</b> " . $webhook_secret . "</p>";
    return ['#markup' => $output];
  }

}
