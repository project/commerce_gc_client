<?php

namespace Drupal\commerce_gc_client\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a confirmation form for performing actions on scheduled adjustments.
 */
class AdjustmentAction extends ConfirmFormBase {

  /**
   * The action to perform on the item.
   *
   * @var string
   */
  protected $action;

  /**
   * The commerce order ID of the item to action.
   *
   * @var int
   */
  protected $orderId;

  /**
   * The Schedule ID of the item to action.
   *
   * @var int
   */
  protected $sid;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_gc_client_acheduled_action';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
        'Are you sure you want to @action the scheduled adjustment?', [
          '@action' => $this->action,
        ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url(
        'commerce_gc_client.mandate', [
          'commerce_order' => $this->orderId,
        ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t(
        '@action scheduled adjustment', [
          '@action' => ucfirst($this->action),
        ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Go back');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $action = NULL, $orderId = NULL, $sid = NULL) {
    $this->action = $action;
    $this->orderId = $orderId;
    $this->sid = $sid;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $db = \Drupal::database();
    if ($this->action == 'cancel') {
      $db->update('commerce_gc_client_item_schedule')->fields(
        [
          'status' => 0,
        ]
      )->condition('sid', $this->sid)->execute();
    }
    elseif ($this->action == 'delete') {
      $db->delete('commerce_gc_client_item_schedule')->condition('sid', $this->sid)->execute();
    }
    $this->action == 'cancel' ? $action = 'cancelled' : $action = 'deleted';
    $this->messenger()->addMessage(
        $this->t(
            'Scheduled adjustment has been @action.', [
              '@action' => $action,
            ]
        )
    );
    $form_state->setRedirect('commerce_gc_client.mandate', ['commerce_order' => $this->orderId], ['query' => ['adjustment' => $this->action]]);
  }

}
