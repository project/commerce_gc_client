<?php

namespace Drupal\commerce_gc_client\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_gc_client\GoCardlessPartner;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form for cancelling pending payments.
 */
class PaymentCancel extends ConfirmFormBase {

  /**
   * The commerce order ID of the item to cancel.
   *
   * @var int
   */
  protected $orderId;

  /**
   * The commerce payment ID of the item to cancel.
   *
   * @var int
   */
  protected $paymentId;

  /**
   * The GoCardless Partner service.
   *
   * @var \Drupal\commerce_gc_client\GoCardlessPartner
   */
  protected $partner;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_gc_client.gocardless_partner')
    );
  }

  /**
   * Constructs a new PaymentCancel object.
   *
   * @param \Drupal\commerce_gc_clinet\GoCardlessPartner $goCardlessPartner
   *   The GoCardless partner service.
   */
  public function __construct(GoCardlessPartner $goCardlessPartner) {
    $this->partner = $goCardlessPartner;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_gc_client_payment_cancel';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to cancel GoCardless payment @paymentId?', [
      '@paymentId' => $this->paymentId,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url(
        'commerce_gc_client.mandate', [
          'commerce_order' => $this->orderId,
        ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Cancel payment');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Go back');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $commerce_order = NULL, $payment_id = NULL) {
    $this->orderId = $commerce_order->id();
    $this->paymentId = $payment_id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $order = Order::load($this->orderId);
    $payment_gateway_id = $order->get('payment_gateway')->first()->entity->Id();
    $this->partner->setGateway($payment_gateway_id);
    $result = $this->partner->api([
      'endpoint' => 'payments',
      'action' => 'cancel',
      'id' => $this->paymentId,
    ]);
    if ($result->response->status_code == 200) {
      $this->messenger()->addMessage($this->t('Payment @paymentId has been cancelled with GoCardless.', ['@paymentId' => $this->paymentId]));
    }
    else {
      $this->messenger()->addWarning($this->t('Something went wrong cancelling paynent @paymentId with GoCardless.', ['@paymentId' => $this->paymentId]));
    }
    $form_state->setRedirect(
      'commerce_gc_client.mandate', [
        'commerce_order' => $this->orderId,
      ]
    );
  }

}
