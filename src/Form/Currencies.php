<?php

namespace Drupal\commerce_gc_client\Form;

use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\commerce_price\Entity\Currency;

/**
 * Defines a form for configurring GoCardless currencies.
 */
class Currencies extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_gc_client_currencies_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $currencies = $this->config('commerce_gc_client.settings')->get('currency_schemes');
    // TODO find easier way of loading enabled currencies.
    foreach ($this->configFactory()->listAll('commerce_price.commerce_currency') as $commerce_currency) {
      $commerce_currency_code = substr($commerce_currency, -3);
      $commerce_currencies[$commerce_currency_code] = Currency::load($commerce_currency_code);
    }

    $countries = CountryManager::getStandardList();
    $rows = [];
    $rows_unavailable = [];
    $defaults = [];
    foreach ($currencies as $currency_code => & $currency) {
      $currency_countries = [];
      foreach ($currency['countries'] as $country_code => $country) {
        $country_name = $countries[$country_code];
        if (isset($country['failure_warning'])) {
          $country_name .= '*';
        }
        $currency_countries[$country_code] = $country_name;
      }
      $currency_countries = implode(', ', $currency_countries);

      $row = [
        $currency_code,
        $currency['scheme'],
        $currency_countries,
      ];

      if (isset($commerce_currencies[$currency_code])) {
        $rows[$currency_code] = $row;
        if (isset($currency['enabled']) && $currency['enabled']) {
          $defaults[$currency_code] = TRUE;
        }
      }
      else {
        $rows_unavailable[$currency_code] = $row;
      }
    }
    $header = [
      $this->t('Currency code'),
      $this->t('Scheme'),
      $this->t('Countries'),
    ];

    $markup = '<p>' . $this->t('Use this form to define which currencies you want to use and / or have enabled with your GoCardless account. Before you can enable a GoCardless currency here, you must first <a href="/admin/commerce/config/currencies">enable it in Commerce</a>.') . '</p>';

    $markup .= '<p>' . $this->t('A filter is added to product variation types that are configured to "Use GoCardless recurrence rules", so that when you are adding / editing a product variant, the range of currencies available is limited to those which you have enabled here. This helps to prevent attempts to checkout using currencies that your GoCardless account is unable to handle.') . '</p>';

    $markup .= '<p>' . $this->t('Also, if an order is created with a currency that is not enabled here, the GoCardless payment gateway is made unavailable to the checkout.') . '</p>';

    $markup .= '<p>' . $this->t('For information on currencies and countries that are serviced by GoCardless <a target="new" href ="https://gocardless.com/faq/merchants/international-payments">view their FAQ page here</a>. To use international payments you must ensure that FX Payments is enabled for your GoCardless account. This is enabled by default in newer accounts but if it is not then contact help@gocardless.com to have it set up.') . '</p>';

    $form['markup'] = ['#markup' => $markup];
    $form['currencies'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => $this->t('No countries available.'),
      '#default_value' => $defaults,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enable / disable currencies'),
    ];
    $form['markup_unavailable'] = [
      '#markup' => '<p>' . $this->t('GoCardless currencies that are not enabled in Commerce:') . '</p>',
    ];
    $form['currencies_unavailable'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows_unavailable,
      '#empty' => $this->t('All currencies available.'),
    ];
    $form['markup_failure_warning'] = [
      '#markup' => '<p style = "font-size: smaller;">' . $this->t('* Debit payments in these countries have a relatively high failure rate.') . '</p>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->configFactory()->getEditable('commerce_gc_client.settings');
    $currencies = $settings->get('currency_schemes');
    $values = $form_state->getValue(['currencies']);
    foreach ($currencies as $currency_code => $currency) {
      $enabled = FALSE;
      if (isset($values[$currency_code]) && $values[$currency_code]) {
        $values[$currency_code] ? $enabled = TRUE : NULL;
      }
      $currencies[$currency_code]['enabled'] = $enabled;
    }
    $settings->set('currency_schemes', $currencies)->save();
    $this->messenger()->addMessage($this->t('Available GoCardless currencies have been updated.'));
  }

}
