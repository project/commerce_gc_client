<?php

namespace Drupal\commerce_gc_client\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a confirmation form for cancelling mandates.
 */
class MandateCancel extends ConfirmFormBase {

  /**
   * The commerce order entity for the item to cancel.
   *
   * @var object
   */
  protected $order;

  /**
   * The commerce user ID of the item to cancel.
   *
   * @var int
   */
  protected $uid;

  /**
   * The order's view mode to return the user to upon completion.
   *
   * @var int
   */
  protected $viewMode;

  /**
   * Details of the mandate to be cancelled.
   *
   * @var array
   */
  protected $mandate;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_gc_client_mandate_cancel';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
        'Are you sure you want to cancel GoCardless mandate @mandate_id?', [
          '@mandate_id' => $this->mandate ? $this->mandate->gc_mandate_id : NULL,
        ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    if ($this->viewMode == 'admin') {
      return new Url(
        'entity.commerce_order.canonical', [
          'commerce_order' => $this->order->id(),
        ]
      );
    }
    else {
      return new Url(
        'entity.commerce_order.user_view', [
          'user' => $this->uid,
          'commerce_order' => $this->order->id(),
        ]
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Cancel mandate');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Go back');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = NULL, $commerce_order = NULL, $viewMode = NULL) {
    $this->uid = $user->id();
    $this->order = $commerce_order;
    $this->viewMode = $viewMode;
    $db = \Drupal::database();
    $this->mandate = $db->select('commerce_gc_client', 'c')
      ->fields('c', ['gcid', 'gc_mandate_id'])
      ->condition('order_id', $this->order->id())
      ->condition('gc_mandate_status', 'cancelled', '!=')
      ->execute()->fetch();
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('commerce_log')) {
      $log_storage = \Drupal::entityTypeManager()->getStorage('commerce_log');
      $log_storage->generate(
        $this->order, 'mandate_cancel', [
          'mandate_id' => $this->mandate->gc_mandate_id,
          'viewMode' => $this->viewMode,
        ]
      )->save();
    }
    commerce_gc_client_mandate_cancel($this->order->id());
    $url = $this->getCancelUrl();
    $form_state->setRedirect($url->getRouteName(), $url->getRouteParameters());
  }

}
