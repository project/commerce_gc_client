<?php

namespace Drupal\commerce_gc_client\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired before a mandate is created with GoCardless.
 */
class MandateDetailsEvent extends Event {

  /**
   * The mandate details array built for GoCardless.
   *
   * @var array
   */
  protected $mandateDetails;

  /**
   * The Commerce order ID.
   *
   * @var int
   */
  protected $orderId;

  /**
   * Constructs the object.
   *
   * @param array $mandateDetails
   *   The mandate details array to be passed to the GoCardless API.
   * @param int $orderId
   *   The Commerce order ID.
   */
  public function __construct(array $mandateDetails, $orderId) {
    $this->mandateDetails = $mandateDetails;
    $this->orderId = $orderId;
  }

  /**
   * Gets the mandate details.
   *
   * @return array
   *   The mandate details array to be passed to the GoCardless API.
   */
  public function getMandateDetails() {
    return $this->mandateDetails;
  }

  /**
   * Sets the mandate details.
   *
   * @param array $mandateDetails
   *   The mandate details array to be passed to the GoCardless API.
   */
  public function setMandateDetails(array $mandateDetails) {
    $this->mandateDetails = $mandateDetails;
  }

  /**
   * Gets the order ID.
   *
   * @return int
   *   The Commerce order ID.
   */
  public function getOrderId() {
    return $this->orderId;
  }

}
