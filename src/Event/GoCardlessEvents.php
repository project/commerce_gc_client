<?php

namespace Drupal\commerce_gc_client\Event;

/**
 * Defines events for the commerce_gc_client module.
 */
final class GoCardlessEvents {

  /**
   * Name of the event fired before creating a mandate.
   *
   * Fired during checkout by the Offsite Payment form, before redirection
   * to GoCardless. Can be used to change the contents of the mandate
   * details array provided to GoCardless.
   *
   * @Event
   *
   * @see \Drupal\commerce_gc_client\Event\MandateDetailsEvent
   */
  const MANDATE_DETAILS = 'commerce_gc_client_mandate_details';

  /**
   * Name of the event fired to proportion shipping costs to an order item.
   *
   * Fired during checkout by the Offsite Payment form, before redirection
   * to GoCardless. Is only fired if the commerce_shipping module is enabled,
   * and can be used to calculate the proportion of an order's shipment cost
   * to a specific order item.
   *
   * @Event
   *
   * @see \Drupal\commerce_gc_client\Event\ShipmentEvent
   * @see \Drupal\commerce_gc_client\EventSubscriber\ShipmentEventSubscriber
   */
  const SHIPMENT = 'commerce_gc_client_shipment';

  /**
   * Name of the event fired when recieving a webhook from GoCardless.
   *
   * The GoCardles webhook array contains one or more 'events', relating to
   * mandates created by the site. The event is fired at the end of
   * processing each webhook event.
   *
   * @Event
   *
   * @see \Drupal\commerce_gc_client\Event\WebhookEvent
   */
  const WEBHOOK = 'commerce_gc_client_webhook';

  /**
   * Name of the event fired before creating a subscription.
   *
   * Fired at end of checkout after redirection from GoCardless back to
   * site, for any order items that are subscriptions. Can be used to change
   * the contents of the subscription details array, before a request is made
    *to the GoCardless API to create a new subscription.
   *
   * @Event
   *
   * @see \Drupal\commerce_gc_client\Event\SubsDetailsEvent
   */
  const SUBS_DETAILS = 'commerce_gc_client_subs_details';

  /**
   * Name of the event fired after creating a payment.
   *
   * Fired in following circumstances:
   * - At end of checkout after redirection from GoCardless back to
   *   site, for any one-off payment order items that are set to create a
   *   payment immediately;
   * - Automatically during cron runs when a payment is created;
   * - Manaually when submitting the 'Create payment' section of a
   *   GoCardless Mandate form.
   *
   * @Event
   *
   * @see \Drupal\commerce_gc_client\Event\PaymentCreatedEvent
   */
  const PAYMENT_CREATED = 'commerce_gc_client_payment_created';

  /**
   * Name of the event fired before creating a payment.
   *
   * Can be used to change the contents of the payment details array, before
   * a request is made to the GoCardless API to create a new payment.
   * Fired in following circumstances:
   * - At end of checkout after redirection from GoCardless back to site,
   *   for any order items that are set to create a payment immediately;
   * - Automatically during cron runs when a payment is created.
   *
   * @Event
   *
   * @see \Drupal\commerce_gc_client\Event\PaymentDetailsEvent
   */
  const PAYMENT_DETAILS = 'commerce_gc_client_payment_details';

  /**
   * Name of the event fired before storing the next_payment date.
   *
   * Can be used to override the next_payment date that is calculated
   * for an order item, based on it's GoCardless recurrence settings.
   * Fired in following circumstances:
   * - At end of checkout after redirection from GoCardless back to site,
   *   for any order items that are of 'One-off payments' type;
   * - Automatically during cron runs when a payment is created.
   *
   * @Event
   *
   * @see \Drupal\commerce_gc_client\Event\PaymentNextEvent
   */
  const PAYMENT_NEXT = 'commerce_gc_client_payment_next';

}
