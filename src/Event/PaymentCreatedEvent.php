<?php

namespace Drupal\commerce_gc_client\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired after a payment has been created at GoCardless.
 */
class PaymentCreatedEvent extends Event {

  /**
   * The GoCardless payment object.
   *
   * @var object
   */
  protected $payment;

  /**
   * The Commerce order item ID.
   *
   * @var int
   */
  protected $itemId;

  /**
   * The context in which the event was dispatched.
   *
   * @var string
   */
  protected $context;

  /**
   * Constructs the object.
   *
   * @param object $payment
   *   The GoCardless payment object.
   * @param int $itemId
   *   The Commerce order item ID.
   * @param string $context
   *   The context in which the event was dispatched.
   */
  public function __construct($payment, $itemId, $context) {
    $this->payment = $payment;
    $this->itemId = $itemId;
    $this->context = $context;
  }

  /**
   * Gets the payment.
   *
   * @return object
   *   The GoCardless payment object.
   */
  public function getPayment() {
    return $this->payment;
  }

  /**
   * Sets the payment.
   *
   * @param object $payment
   *   The payment object provided by GoCardless.
   */
  public function setPayment($payment) {
    $this->payment = $payment;
  }

  /**
   * Gets the item ID.
   *
   * @return int
   *   The Commerce order item ID.
   */
  public function getItemId() {
    return $this->itemId;
  }

  /**
   * Gets the Context.
   *
   * @return string
   *   The context in which the event was dispatched.
   */
  public function getContext() {
    return $this->context;
  }

}
