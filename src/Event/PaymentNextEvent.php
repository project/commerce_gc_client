<?php

namespace Drupal\commerce_gc_client\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is called before the next payment date is updated.
 */
class PaymentNextEvent extends Event {

  /**
   * The nextPayment date as a Unix timestamp.
   *
   * @var int
   */
  protected $nextPayment;

  /**
   * The Commerce order item ID.
   *
   * @var int
   */
  protected $itemId;


  /**
   * The context in which the event was dispatched.
   *
   * @var string
   */
  protected $context;

  /**
   * Constructs the object.
   *
   * @param int $nextPayment
   *   The nextPayment date as a Unix timestamp.
   * @param int $itemId
   *   The Commerce order item ID.
   * @param string $context
   *   The context in which the event was dispatched.
   */
  public function __construct($nextPayment, $itemId, $context) {
    $this->nextPayment = $nextPayment;
    $this->itemId = $itemId;
    $this->context = $context;
  }

  /**
   * Gets the next payment.
   *
   * @return int
   *   The nextPayment date as a Unix timestamp.
   */
  public function getNextPayment() {
    return $this->nextPayment;
  }

  /**
   * Sets the next payment.
   *
   * @param int $nextPayment
   *   The nextPayment date as a Unix timestamp.
   */
  public function setNextPayment($nextPayment) {
    $this->nextPayment = $nextPayment;
  }

  /**
   * Gets the item id.
   *
   * @return int
   *   The commerce order item ID.
   */
  public function getItemId() {
    return $this->itemId;
  }

  /**
   * Gets the Context.
   *
   * @return string
   *   The context in which the event was dispatched.
   */
  public function getContext() {
    return $this->context;
  }

}
