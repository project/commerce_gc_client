<?php

namespace Drupal\commerce_gc_client\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired after a webhook is received from GoCardless.
 */
class WebhookEvent extends Event {

  /**
   * The webhook event provided by GoCardless.
   *
   * @var array
   */
  protected $webhookEvent;

  /**
   * A resource relating to a webhook event from GoCardless.
   *
   * Applies to 'payments' type events only.
   *
   * @var array
   */
  protected $resource;

  /**
   * The Commerce order ID that the webhook event relates to.
   *
   * @var int
   */
  protected $orderId;

  /**
   * Constructs the object.
   *
   * @param array $webhookEvent
   *   The webhook event provided by GoCardless.
   * @param array|null $resource
   *   Array containing a "resource" relating to a GoCardless Payments webhook
   *   event, or NULL for other types of event.
   * @param int $orderId
   *   The Commerce order ID that the webhook event relates to.
   */
  public function __construct(array $webhookEvent, $resource, $orderId) {
    $this->webhookEvent = $webhookEvent;
    $this->resource = $resource;
    $this->orderId = $orderId;
  }

  /**
   * Gets the webhook event.
   *
   * @return array
   *   The webhook event provided by GoCardless.
   */
  public function getWebhookEvent() {
    return $this->webhookEvent;
  }

  /**
   * Gets the resource.
   *
   * @return array
   *   A resource relating to a webhook event from GoCardless.
   */
  public function getResource() {
    return $this->resource;
  }

  /**
   * Gets the order ID.
   *
   * @return int
   *   The Commerce order ID that the webhook event relates to.
   */
  public function getOrderId() {
    return $this->orderId;
  }

}
