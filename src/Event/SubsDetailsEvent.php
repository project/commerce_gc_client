<?php

namespace Drupal\commerce_gc_client\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired before a subscription is created with GoCardless.
 */
class SubsDetailsEvent extends Event {

  /**
   * The subscription details array built for GoCardless.
   *
   * @var array
   */
  protected $subsDetails;

  /**
   * The Commerce order item ID.
   *
   * @var int
   */
  protected $itemId;

  /**
   * Constructs the object.
   *
   * @param array $subsDetails
   *   The subscription details array to be passed to the GoCardless API.
   * @param int $itemId
   *   The Commerce order item ID.
   */
  public function __construct(array $subsDetails, $itemId) {
    $this->subsDetails = $subsDetails;
    $this->itemId = $itemId;
  }

  /**
   * Gets the subscription details.
   *
   * @return array
   *   The subscription details array to be passed to the GoCardless API.
   */
  public function getSubsDetails() {
    return $this->subsDetails;
  }

  /**
   * Sets the subscription details.
   *
   * @param array $subsDetails
   *   The subscription details array to be passed to the GoCardless API.
   */
  public function setSubsDetails(array $subsDetails) {
    $this->subsDetails = $subsDetails;
  }

  /**
   * Gets the item ID.
   *
   * @return int
   *   The Commerce order item ID.
   */
  public function getItemId() {
    return $this->itemId;
  }

}
