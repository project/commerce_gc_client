<?php

namespace Drupal\commerce_gc_client\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired from the Offsite Payment form.
 *
 * Calculates a proportion of the shipment amount to the order item.
 */
class ShipmentEvent extends Event {

  /**
   * The Commerce Shipping shipment entity.
   *
   * @var object
   */
  protected $shipment;

  /**
   * The Commerce order item entity.
   *
   * @var object
   */
  protected $orderItem;

  /**
   * If the order item is a shipment item.
   *
   * @var bool
   */
  protected $orderItemShipped = FALSE;

  /**
   * An item's proportion of the total shipment price for an order.
   *
   * @var float
   */
  protected $itemShipmentProportion = 0;

  /**
   * The sum of the quantities of items in an order.
   *
   * @var int
   */
  protected $shipmentItemsQty = 0;

  /**
   * Constructs the object.
   *
   * @param object $shipment
   *   The Commerce Shipping shipment entity.
   * @param object $orderItem
   *   The Commerce order item entity.
   */
  public function __construct($shipment, $orderItem) {
    $this->shipment = $shipment;
    $this->orderItem = $orderItem;
    $orderItem_id = $orderItem->id();
    foreach ($shipment->getItems() as $shipment_item) {
      if ($orderItem_id == $shipment_item->getOrderItemId()) {
        $this->orderItemShipped = TRUE;
      }
      $this->shipmentItemsQty += $shipment_item->getQuantity();
    }
  }

  /**
   * Gets the shipment entity.
   *
   * @return object
   *   The Commerce Shipping shipment entity.
   */
  public function getShipment() {
    return $this->shipment;
  }

  /**
   * Gets the shipment items quantity.
   *
   * @return int
   *   The sum of the quantities of items in an order.
   */
  public function getShipmentItemsQty() {
    return $this->shipmentItemsQty;
  }

  /**
   * Gets the order item shipped boolean.
   *
   * @return bool
   *   If the order item is a shipment item.
   */
  public function getOrderItemShipped() {
    return $this->orderItemShipped;
  }

  /**
   * Gets the order item entity.
   *
   * @return object
   *   The Commerce order item.
   */
  public function getOrderItem() {
    return $this->orderItem;
  }

  /**
   * Gets the item shipment proportion.
   *
   * @return float
   *   The calculated shipment proportion for the item.
   */
  public function getItemShipmentProportion() {
    return $this->itemShipmentProportion;
  }

  /**
   * Sets the item shipment proportion.
   *
   * @param float $itemShipmentProportion
   *   The calculated shipment proportion for the item.
   */
  public function setItemShipmentProportion($itemShipmentProportion) {
    $this->itemShipmentProportion = $itemShipmentProportion;
  }

}
