<?php

namespace Drupal\commerce_gc_client\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired before a payment is created with GoCardless.
 */
class PaymentDetailsEvent extends Event {

  /**
   * The payment details array built for GoCardless.
   *
   * @var array
   */
  protected $paymentDetails;

  /**
   * The Commerce order item ID.
   *
   * @var int
   */
  protected $itemId;

  /**
   * The context in which the event was dispatched.
   *
   * @var string
   */
  protected $context;

  /**
   * Constructs the object.
   *
   * @param array $paymentDetails
   *   The payment details array to be passed to the GoCardless API.
   * @param int $itemId
   *   The Commerce order item ID.
   * @param string $context
   *   The context in which the event was dispatched.
   */
  public function __construct(array $paymentDetails, $itemId, $context) {
    $this->paymentDetails = $paymentDetails;
    $this->itemId = $itemId;
    $this->context = $context;
  }

  /**
   * Gets the payment details.
   *
   * @return array
   *   The payment details array to be passed to the GoCardless API.
   */
  public function getPaymentDetails() {
    return $this->paymentDetails;
  }

  /**
   * Sets the payment details.
   *
   * @param array $paymentDetails
   *   The payment details array to be passed to the GoCardless API.
   */
  public function setPaymentDetails(array $paymentDetails) {
    $this->paymentDetails = $paymentDetails;
  }

  /**
   * Gets the item ID.
   *
   * @return int
   *   The Commerce order item ID.
   */
  public function getItemId() {
    return $this->itemId;
  }

  /**
   * Gets the Context.
   *
   * @return string
   *   The context in which the event was dispatched.
   */
  public function getContext() {
    return $this->context;
  }

}
