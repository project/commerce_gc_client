<?php

namespace Drupal\commerce_gc_client\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_gc_client\Event\GoCardlessEvents;
use Drupal\commerce_gc_client\Event\ShipmentEvent;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\commerce_gc_client\EventSubscriber
 */
class ShipmentEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      GoCardlessEvents::SHIPMENT => 'shipment',
    ];
  }

  /**
   * Adds GoCardless specific shipping data to the order item data array.
   */
  public function shipment(ShipmentEvent $event) {
    if ($event->getOrderItemShipped()) {
      return;
    }
    $shipment = $event->getShipment();
    $plugin_id = $shipment->getShippingMethod()->getPlugin()->getPluginId();
    $order_item_id = $event->getOrderItem()->id();
    if (in_array($plugin_id, ['flat_rate', 'flat_rate_per_item'])) {
      foreach ($shipment->getItems() as $shipment_item) {
        if ($order_item_id == $shipment_item->getOrderItemId()) {
          $shipment_item_qty = $shipment_item->getQuantity();
          $shipment_items_qty = $event->getShipmentItemsQty();
          $proportion = round($shipment_item_qty / $shipment_items_qty, 6);
          $event->setItemShipmentProportion($proportion);
        }
      }
    }
  }

}
