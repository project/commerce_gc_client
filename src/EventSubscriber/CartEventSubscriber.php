<?php

namespace Drupal\commerce_gc_client\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartEvents;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\commerce_gc_client\EventSubscriber
 */
class CartEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      CartEvents::CART_ENTITY_ADD => 'orderItemSetData',
    ];
  }

  /**
   * Adds GoCardless specific data to the OrderItem object.
   *
   * @param \Drupal\commerce_cart\Event\CartEntityAddEvent $event
   *   The add to cart event.
   */
  public function orderItemSetData(CartEntityAddEvent $event) {
    $variation_id = $event->getEntity()->get('variation_id')->value;
    $connection = \Drupal::service('database');
    $gc = $connection->select('commerce_gc_client_variation', 'v')
      ->fields('v', ['data'])
      ->condition('variation_id', $variation_id)
      ->condition('gc_use', TRUE)
      ->execute()->fetchField();

    // Only add extra data if GoCardless is enabled for product.
    if (!$gc) {
      return;
    }

    $gc = unserialize($gc);
    $interval_params = [];

    if ($gc['gc_interval_length']) {
      $interval_params['length'] = $gc['gc_interval_length'];
      $interval_params['unit'] = $gc['gc_interval_unit'];
    }

    if (!empty($interval_params)) {
      $interval_params['string'] = $interval_params['length'] . ' ' . str_replace("ly", "", $interval_params['unit']);
      $interval_params['gc'] = $interval_params['length'] . ' ' . $interval_params['unit'];
      $gc['interval_params'] = $interval_params;
    }

    $event->getOrderItem()->setData('gc', $gc)->save();
  }

}
