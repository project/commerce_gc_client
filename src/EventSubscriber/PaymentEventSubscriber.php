<?php

namespace Drupal\commerce_gc_client\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_payment\Event\PaymentEvents;
use Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\commerce_gc_client\EventSubscriber
 */
class PaymentEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      PaymentEvents::FILTER_PAYMENT_GATEWAYS => 'filterPaymentGateways',
    ];
  }

  /**
   * Filters payment gateways.
   *
   * The GoCardless gateway(s) are filtered out if an order's currency is not
   * supported by the client's GoCardless account.
   *
   * @param \Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent $event
   *   The filter payment gateways event.
   */
  public function filterPaymentGateways(FilterPaymentGatewaysEvent $event) {
    $order = $event->getOrder();
    $order_currency_code = $order->getTotalPrice()->getCurrencyCode();
    $gocardless_currencies = \Drupal::config('commerce_gc_client.settings')->get('currency_schemes');
    if (isset($gocardless_currencies[$order_currency_code]) &&
        $gocardless_currencies[$order_currency_code]['enabled']) {
      // Do nothing.
    }
    else {
      // Hide the gateway.
      $gateways = $event->getPaymentGateways();
      foreach ($gateways as $gateway_id => $gateway) {
        if ($gateway->getPluginId() == 'gocardless_client') {
          unset($gateways[$gateway_id]);
          $event->setPaymentGateways($gateways);
          \Drupal::logger('commerce_gc_client')->warning('The GoCardless payment gateway was hidden because the order currency is not supported by your GoCardless account.', []);
        }
      }
      if (!$gateways) {
        \Drupal::messenger()->addWarning(t('It is not possible to checkout with GoCardless because the currency of your order is incompatible.'));
      }
    }
  }
}

