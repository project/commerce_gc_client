<?php

namespace Drupal\commerce_gc_client\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for connecting with the GoCardless partner: Seamless-CMS.co.uk.
 */
class GoCardlessPartnerConnect extends ControllerBase {

  /**
   * Callback function: Saves key variables for connecting with Partner site.
   *
   * Variables are posted here from Partner site, following completion of
   * GoCardless OAuth flow.
   */
  public function connect() {
    if (isset($_POST['environ'])) {
      $ext = $_POST['environ'] == 'SANDBOX' ? '_sandbox' : '_live';
      $settings = \Drupal::service('config.factory')->getEditable('commerce_gc_client.settings');

      // Check that the  returned password matches the saved one before
      // proceeding, and return error 403 Forbidden if it does not.
      if (isset($_POST['pass']) && $_POST['pass'] == $settings->get('partner_pass' . $ext)) {
        if (isset($_POST['id'])) {
          $settings->set('org_id' . $ext, $_POST['id'])->save();
        }
        if (isset($_POST['name'])) {
          $settings->set('partner_user' . $ext, $_POST['name'])->save();
        }
      }
      else {
        return new Response(
        'Forbidden', 403, [
          'Content-Type' => 'text/html',
        ]);
      }
    }
    return new Response();
  }

  /**
   * Redirects user to GC settings page upon completion of OAuth flow.
   */
  public function connectComplete() {
    $success = false;
    if (isset($_GET['status'])) {
      if ($_GET['status'] == 'insecure') {
        \Drupal::messenger()->addError(t('Connection cannot be created because site must be secure (https) to use LIVE environment'));
      }
      elseif ($_GET['status'] == 'connected') {
        $success = true;
      }

      // Status will be failed if the wrong passward is provided in the request
      // or if trying to connect from a site that is not on the internet.
      elseif ($_GET['status'] == 'failed') {

        // Since connection credentials failed to be set previously then do it
        // now instead. This method is only available in sandbox mode, since
        // it is not safe to pass sensitive data via url parameters.
        if (isset($_GET['connect'])) {
          $settings = \Drupal::service('config.factory')->getEditable('commerce_gc_client.settings');
          $settings->set('partner_user_sandbox', $_GET['connect'])->save();
          $exploded = explode("_", $_GET['connect']);
          if (isset($exploded[2])) {
            $settings->set('org_id_sandbox', $exploded[2])->save();
            $success = true;
          }
        }
      }
    }
    if ($_GET['status'] !== 'insecure') {
      $this->message($success);
    }

    // Unset session variables to reduce possibility of authentication failure
    // if client is reconnecting.
    $session = \Drupal::request()->getSession();
    if ($session->get('commerce_gc_client_cookie_created')) {
      $session->remove('commerce_gc_client_cookie_created');
    }

    if (isset($_GET['gateway_id'])) {
      $path = Url::fromRoute('entity.commerce_payment_gateway.edit_form', [
        'commerce_payment_gateway' => $_GET['gateway_id'],
      ], ['absolute' => TRUE])->toString();
      return new RedirectResponse($path);
    }
  }

  /**
   * Inform user of connection result.
   */
  private function message($success) {
    if ($success) {
      \Drupal::messenger()->addMessage(t('You have connected successfully with GoCardless'));
    }
    else {
      \Drupal::messenger()->addError(t('Connection with GoCardless has failed'));
    }
  }

}
