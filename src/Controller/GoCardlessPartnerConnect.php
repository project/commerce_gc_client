<?php

namespace Drupal\commerce_gc_client\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for connecting with the GoCardless partner: Seamless-CMS.co.uk.
 */
class GoCardlessPartnerConnect extends ControllerBase {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  
  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;
  
  /**
   * Constructs the GoCardlessPartner object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messengerInterface
   *   The messenger interface.
   * @param Symfony\Component\HttpFoundation\RequestStack $current_request
   *   The Symfony request stack component.
   */
  public function __construct(MessengerInterface $messenger_interface, RequestStack $request_stack) {
    $this->messenger = $messenger_interface;
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('request_stack')
    );
  }

  /**
   * Redirects user to the GoCardless payment gateway settings page.
   *
   * Fired upon completion of OAuth flow.
   */
  public function complete() {
    $status = $this->currentRequest->query->get('status');
    if ($status == 'connected') {
      $this->messenger->addMessage(t('You have connected successfully with GoCardless'));
    }
    elseif ($status == 'insecure') {
      $this->messenger->addError(t('Connection cannot be created because site must be secure (https) to use LIVE environment'));
    }
    else {
      $this->messenger->addError(t('There was a problem connecting the site with Seamless-CMS and GoCardless. Please try again later.'));
    }

    // Unset session variables to reduce possibility of authentication failure
    // if client is reconnecting.
    $session = $this->currentRequest->getSession();
    $session->remove('commerce_gc_client_cookie_created');

    if ($gateway_id = $this->currentRequest->query->get('gateway_id')) {
      $path = Url::fromRoute('entity.commerce_payment_gateway.edit_form', [
        'commerce_payment_gateway' => $gateway_id,
      ], ['absolute' => TRUE])->toString();
      return new RedirectResponse($path);
    }
  }

}
