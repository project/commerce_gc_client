<?php

namespace Drupal\commerce_gc_client\PluginForm;

use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\UrlHelper;
use Drupal\commerce_gc_client\Event\GoCardlessEvents;
use Drupal\commerce_gc_client\Event\MandateDetailsEvent;
use Drupal\commerce_gc_client\Event\ShipmentEvent;

/**
 * Defines the payment gateway's off-site payment form.
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $form = parent::buildConfigurationForm($form, $form_state);
    $payment = $this->entity;
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $mode = $payment_gateway_plugin->getMode();
    $order = $payment->getOrder();
    $order_id = $order->Id();
    $problem_url = Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $order_id,
      'step' => 'order_information',
    ], ['absolute' => TRUE])->toString();

    // Obtain shipping data for order.
    $shipment = FALSE;
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('commerce_shipping')) {
      $db = \Drupal::database();
      if ($shipment_id = $db->select('commerce_shipment', 's')
        ->fields('s', ['shipment_id'])
        ->condition('order_id', $order_id)
        ->execute()->fetchField()) {
        $shipment = Shipment::Load($shipment_id);
      }
    }

    $calculated_total = 0;
    foreach ($order->getItems() as $item) {
      $calculate = commerce_gc_client_price_calculate($order, $item);
      if ($warning = $calculate['warning']) {
        \Drupal::messenger()->addWarning($warning);
        $this->buildRedirectForm($form, $form_state, $problem_url, []);
      }
      $calculated_total += $calculate['amount'];

      if ($shipment) {
        // Dispatch an event so that the shipping price per item can be
        // added to the order item's data array.
        $event = new ShipmentEvent($shipment, $item);
        $event_dispatcher->dispatch($event, GoCardlessEvents::SHIPMENT);
        if ($shipment_proportion = $event->getItemShipmentProportion()) {
          if (!$data = $item->getData('gc')) {
            $data = [];
          }
          $data['shipment'] = [
            'plugin_id' => $shipment->getShippingMethod()->getPlugin()->getPluginId(),
            'proportion' => $shipment_proportion,
          ];
          $item->setData('gc', $data)->save();
        }
      }
    }

    $currency_code = $calculate['currency_code'] ? $calculate['currency_code'] : $order->getTotalPrice()->getCurrencyCode();
    $partner = \Drupal::service('commerce_gc_client.gocardless_partner');
    $partner->setGateway($this->entity->getPaymentGatewayId());
    $redirect_url = $partner->settings['partner_url'] . '/gc_partner/mandate?payment_method=gocardless_client&org_id=' . $partner->settings['org_id_' . $mode] . '&order_id=' . $order_id . '&complete_path=' . $form['#return_url'];

    // Generate the mandate_details array.
    $user = ['email' => $order->getEmail()];
    if ($profile = $order->getBillingProfile()) {
      $billing_country = $profile->address->country_code;
      $user += [
        'first_name' => $profile->address->given_name,
        'last_name' => $profile->address->family_name,
        'company_name' => $profile->address->organisation,
        'billing_address1' => $profile->address->address_line1,
        'billing_address2' => $profile->address->address_line2,
        'billing_town' => $profile->address->locality,
        'billing_postcode' => $profile->address->postal_code,
        'billing_country' => $billing_country,
      ];
      $user = array_map(
        function ($str) {
          return $str ? $str : '';
        }, $user
      );
    }

    // Determine if a Billing Request flow or Redirect flow.
    $mandate = FALSE;
    $billing_request = TRUE;
    foreach ($order->getItems() as $item) {
      // If the currency is neither GBP or EUR use the Redirect Flow 
      if ($currency_code != 'GBP' && $currency_code != 'EUR') {
        $billing_request = FALSE;
        break;
      }
      // If the currency is EUR but the customer is not German then use
      // Redirect Flow. 
      elseif ($currency_code == 'EUR' && isset($billing_country) && $billing_country !== 'DE') {
        $billing_request = FALSE;
        break;
      }
      // Else determine flow based on cart contents.
      // The Billing Request flow is only required if all the cart items are 
      // elligible for an instant payment. This includes products of "Instant 
      // Payment" type, and also the "One-off" type but only if configured to 
      // create a payment immediately on checkout. Carts that contain a 
      // "Subscription" product always use the Redirect flow. Carts that contain
      // either a "Subscription", or a "One-off" payments product always require
      // a mandate. 
      else {
        if ($gc = $item->getData('gc')) {
          $type = $gc['gc_type'];
          if ($type == 'S') {
            $mandate = TRUE;
            $billing_request = FALSE;
            break;
          }
          elseif ($type == 'P') {
            $mandate = TRUE;
            if (!$gc['gc_create_payment']) {
              $billing_request = FALSE;
            }
          }
        }
      }
    }

    // Billing Request Flow
    if ($billing_request) {
      $data = [ 
        'payment_request' => [
          'description' => 'Order #' . $order_id,
          'amount' => $calculated_total * 100,
          'currency' => $calculate['currency_code'],
        ],
        'billing_request_details' => [
          'user' => $user,
        ],
      ];

      // TODO add an event here so that other modules can input before
      // redirection to GoCardless.

      global $base_url;
      $data = array_merge([ 
        'endpoint' => 'billing_request_flows',
        'action' => 'create',
        'order_id' => $order_id,
        'base_url' => $base_url,
        'redirect_path' => '/checkout/' . $order_id . '/payment/return',
        'exit_path' => '/checkout/' . $order_id . '/payment/cancel',
      ], $data);
      if ($mandate) {
        $data['mandate_request'] = ['scheme' => $calculate['scheme']];
      }
    }

    // Redirect Flow
    else {
      $mandate_details = [
        'name' => 'Order #' . $order_id,
        'scheme' => !$partner->settings['configuration']['currencies'] ? $calculate['scheme'] : NULL,
        'redirect_url' => $redirect_url,
        'user' => $user,
      ];
      // Dispatch an event so that the mandate details array can be altered
      // by other modules before sending to GoCardless.
      $mandate_details_event = new MandateDetailsEvent($mandate_details, $order_id);
      $event_dispatcher->dispatch($mandate_details_event, GoCardlessEvents::MANDATE_DETAILS);
      $mandate_details = $mandate_details_event->getMandateDetails();
      $data = [
        'endpoint' => 'redirect_flows',
        'action' => 'create',
        'mandate_details' => $mandate_details,
        'order_id' => $order_id,
      ];
    }

    $result = $partner->api($data);
    if (isset($result->response) && UrlHelper::isValid($result->response)) {
      $redirect_url = $result->response;
      $this->buildRedirectForm($form, $form_state, $redirect_url, []);
    }
    else {
      \Drupal::messenger()->addError($this->t('Something went wrong during checkout with GoCardless and your order has not been completed.'));
      $this->buildRedirectForm($form, $form_state, $problem_url, []);
    }
    return $form;
  }

}
