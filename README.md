FILE CONTENTS
=============
   
 * Introduction
 * Product Payment Types
 * Requirements
 * Installation
 * Configuration
 * Recurring order
 * International payments
 * Commerce Currency Resolver module
 * International payments and Subscriptions
 * Shipping
 * Known Issues
 * Maintainers


INTRODUCTION
------------

The Commerce GoCardless Client module provides an integration with Drupal
Commerce, and GoCardless.com. The module integrates into Commerce like any 
other payment service, and enables customers to checkout and make Instant 
payments using open banking protocol, and/or create debit mandates for 
generating recurring payments. GoCardless is very competitive compared with
other payment services and charges 1% on transactions, with a minimum of 20p
in the UK, and a similar amount in other countries.

This module is provided by Seamless-CMS which is a <a href="https://gocardless.com/partners/category/e-commerce">partner of GoCardless</a>.
As a partner Seamless-CMS receives a percentage of any revenue collected from
payments. This arrangement does not cost client sites, or end customers any
extra, and it is intended that the income derived here will make the development
and maintenance of the module sustainable.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/commerc_gc_client

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/commerce_gc_client


PRODUCT PAYMENT TYPES
---------------------

Product variations in your store are individually configured to use either
Instant payments, or one of the two recurring payment types: One-off payments,
or Subscription payments.

Instant payments are single payments created upon checkout by GoCardless using
open banking protocol and do not require a debit mandate. Open banking does not
require credit/debit cards but the experience is similar to other online payment
services in that the customer is required to provide their bank details, and to 
verify the bank account.

Subscription and One-off payments both require a debit mandate, which is created
by GoCardless upon checkout. Subscriptions are recurring payments that are
created automatically by GoCardless according to the rules that you set for the
product. One-off payments are (recurring) payments that are created 
automatically when your website instructs GoCardless to do so, and are more
flexible than Subscription payments. Debit payments take several days to
complete following an instruction from GoCardless to the bank to create the
payment.

Instant payments are only created if none of the products in a cart are using
recurrence rules, or if they are One-off payments that are configured to 
'Create a payment immediately' upon checkout. Instant payments are currently
only available for customers with British or German bank accounts. If it is not
possible to create an Instant payment upon checkout, a debit mandate will be
created instead, and the product will be paid for with a One-off payment.


REQUIREMENTS
------------

 * An SSL certificate (https) is required in order to use the module in
   live mode although it is not required for sandbox.

 * This module requires the following modules:

   - Commerce Payment
   - Commerce Product
   - Commerce Cart


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8 for further information.


CONFIGURATION
-------------

 * Install and enable the Commerce payment service in the normal way:
   Admin > Commerce > Configuration > Payment-gateways. 

 * Before you can use your site with GoCardless you need to 'Connect' as a 
   client of Seamless CMS. This is initiated from the payment gateway's 
   configuration page. To do this, click on the 'Connect' button and follow
   the steps. You will be redirected to GoCardless.com. If you do not have an 
   account with GoCardless at this point, you will be required to create one. 
   After this you will be redirected a second time, to Seamless-CMS.co.uk as 
   the final stage in the Connection process, before being returned back to 
   your site.

   It is also important that you set up your GoCardless account properly so
   that you will receive webhooks. Instructions for this are included on the
   payment gateway configuration form.

 * The module ships with 'GoCardless' Product, and Product Variation types.
   You do not have to use these but they provide an example of how to
   configure your store to use the module. Each product variation type has a 
   checkbox: 'Use GoCardless recurrence rules ..'. When you enable 
   this for a specific type, a set of GoCardless recurrence options are 
   provided in the Edit form for product variations of this type.

 * Make sure that the Product types that use the Product Variation types with
   GoCardless recurrence rules enabled, all have "Allow each product to have
   multiple variations" checked. This is required for the 'Variations' tab to
   be visible for each of your products, which is were the GoCardless
   recurrence rules are set.   

 * Product variations are individually configured as Instant payments, or as
   Subscription or One-off payments, along with the required recurrence rules.
   An order item does not need to be using recurrence rules to be included in a
   GoCardless checkout. Such items will be treated as 'One-off payment' types,
   with a single payment being created immediately upon checkout, and no
   recurring payments scheduled.

 * Several 'Events' are provided to enable other modules to interact at key
   points, such as before setting up a new debit mandate, before 
   creating a payment, or after receiving a webhook from GoCardless.
   More information on using these is provided in GoCardlessEvents.php.


RECURRING ORDERS
----------------

As of version 2.0.6 a facility for Recurring Orders is provided. These are
created automatically as children of the parent order when a recurring payment
is created. Recurring Orders are currently only available for One-off payment
types. Product variations are configurred individually to create Recurring 
Orders, or not, within their GoCardless settings section. Child orders
do not create any payments with GoCardless, but are useful for sites where
an individual order number is required for each recurring payment. Product
variations can also be configurred to send a new order confirmation / invoice
to the customer upon creation of the recurring order. 

For existing order items (created pre version 2.0.6) that are using GoCardless
recurrence rules, it is possible to "turn on" Recurring Orders from the
Mandate Administration form. (Go to an order and click on the GoCardless tab.)
Click on "Update recurring order creation" and choose the Recurring Order
options that you desire for the order item. Doing this will not create child
orders for existing payments, but all future payments will have one generated.

Parent and child orders referrence each other, so it is simple to see where a
Recurring Order was generated, and to see which orders a parent has spawned.


INTERNATIONAL PAYMENTS
----------------------

GoCardless can be used with an expanding range of currencies, and even if
your store only uses one currency you can still use the whole range of
GoCardless currencies if you wish, to increase your market. So for example
if your product's currency is GBP (British Pound), and your customer is from,
an EU nation, then the debit mandate can be created using the SEPA 
scheme, in which case payments will be created in EUR. In these circumstances,
the module uses the latest real currency exchange rates provided by GoCardless
to automatically calculate the prices of order items. 

In order for international payments to work you need to have FX Payments
enabled for your GoCardless account. For newer accounts this will be enabled by
default, but if you have an older account you may have to contact 
help@gocardless.com and ask them to enable it.

There is a form at Admin > Commerce > Config > Store > GoCardless currencies.
Use this to select which currencies you have enabled with GoCardless. A filter
is added to product variation types that are configured to "Use GoCardless
recurrence rules", so that when you are adding / editing a product variant,
the range of currencies available is limited to those which you have enabled
at the GoCardless Currencies form. This helps to prevent attempts to checkout
using currencies that your GoCardless account is unable to handle.

For information on currencies and countries that are serviced by GoCardless
view their FAQ page here: https://gocardless.com/faq/merchants/international-payments.


COMMERCE CURRENCY RESOLVER MODULE
---------------------------------

If you are using the Currency Resolver module there is a GoCardless exchange
rate plugin available at Admin > Commerce > Config > Store > Exchange rates.
You can use this to get up to date exchange rates provided by GoCardless, or 
optionally you can use one of the other plugins that ship with the Commerce 
Exchanger module. (You must "Connect" your site as a GoCardless client before
this plugin will work.)

If you choose to use the GoCardless plugin then set it up as follows:
- Click "+Add Exchange rates"
- Provide a label, choose the GoCardless plugin, click 'Enterprise', and ignore
  'Base currency'.
- Save the settings and click "+Run import".

Note that GoCardless only provides exchange rates for the currencies that it
uses, so if your store is using additional currencies then you should choose
a different plugin.

You can use the Currency Resolver module alongside this module's built in
international payment features. An example of how this might work: a product
is priced in EUR, but upon adding to cart the Currency Resolver recalculates
the price to GBP, due to the customer's location. The customer is however
a US citizen, and upon checkout with GoCardless they choose to pay with USD,
and the price of the product is recalculated a second time from GBP using the
real exchange rates provided by GoCardless. 


INTERNATIONAL PAYMENTS AND SUBSCRIPTIONS
----------------------------------------

It is not recommended that you use GoCardless Subscription payments if you are
using the module's built in international payment features, or if you are using
the Commerce Currency Resolver module.

Although it will still work the problem is that if you create a subscription
with GoCardless, and an international currency, the amount provided will be
calculated at current exchange rates, and all future payments created by
GoCardless with the subscription will be for the same amount, and will not
take into account ongoing currency fluctuations. It is possible to change the
price of a subscription after it has been created with GoCardless but they only
allow you to do this twice. 

If you are using international payment features you are better off configuring
your product variants to use scheduled "One-off" payments instead, since these
are triggered by the web site, and the price is recalculated each time using
the latest exhange rates.


COMMERCE SHIPPING MODULE
------------------------
GoCardless Client allows you to check out with a range of products in the 
cart, and in such cases, all products are paid for using the same debit 
mandate. However since products are likely to be a mixture of One-off, 
and Subscription payments, and to have different recurrance rules, this 
creates issues for shipping costs. To manage this, during checkout, the 
proportion of the total shipping cost for the order is calculated for each
order item, and saved for future use when creating payments for items.

Upon completion of checkout, it is possible to access the GoCardless details
for the order, and there is form where you can review and alter the 
proportion of the order's total shipping cost for each order item. 

The module provides an 'Event', which can be hooked into to calculate the
proportion of the shipping for each order item. The module also includes an
Event Subscriber that works out of the box, for 'flat_rate', and 
'flat_rate_per_item' shipment plugins. Developers can use this as a model, 
and override it with your own event subscriber, if you need more bespoke 
functionality, or need this to work with different shipment plugins.


KNOWN ISSUES
------------
The module provides information on the GoCardless mandate for an order, in 
it's 'View' page (in both Admin and User view modes). A mandate cancellation
link is also available. The extra information is included in a form 'Detail', 
but at the moment this Detail is not available out of the box. Use the patch
at https://www.drupal.org/project/commerce/issues/2915559 to make this work in
Admin view mode. There is currently no patch for the User view mode, but
applying the same code in the patch for the relevant template for the User 
view mode also works.


MAINTAINERS
-----------

Current maintainer:
 * Rob Squires (roblog) - https://www.drupal.org/u/roblog
